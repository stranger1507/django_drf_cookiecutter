# New projects template #

## General Information ##
[Cookicutter](https://bitbucket.org/stranger1507/django_drf_cookiecutter/src/master/) template to simplify a new project repo creation. Enter one command and get the predefined structure for your project with the following things:
 - Django project;
 - Django Rest Framework;
 - Docker;
 - UWSGI;
 - NGINX;
 - Postgresql etc.

You can either install all the dependencies inside virtualenv if you prefer this way or use Docker compose file. For additional commands and information look at README of the generated project.

**Note** | This template includes Django v1.11, so you must use the appropriate Python version.
Also note that this is the last Django version supporting Python2. For details look through [Django release notes](https://docs.djangoproject.com/en/1.11/releases/1.11/)

### Usage ###

    Install [cookiecutter](https://bitbucket.org/stranger1507/django_drf_cookiecutter/src/master/) package if you have not installed it yet:

```sh
pip install -U cookiecutter
```

Create your repo from this template:

```sh
cookiecutter git@bitbucket.org:stranger1507/django_drf_cookiecutter.git
```

Then type in your cli asked options.
