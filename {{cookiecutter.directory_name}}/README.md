# {{cookiecutter.project_name|title}} #

1. [General Information](#general-information)
2. [Dependencies](#dependencies)
3. [Installation](#installation)
4. [Development](#development)
5. [Links to servers](#links)


## General Information ##

[{{cookiecutter.project_name}}]() is a REST API.
{{cookiecutter.short_description|capitalize}}

## Dependencies ##

Take a look at *requirements.txt* for Python dependencies.

## Installation ##

run once:

```sh
$ make docker_dev
```

## Development ##

If you use Docker Django app will be exposing on 9000 port by default. It's up to you to change settings.

run docker compose:
```sh
$ make docker_dev
```

rebuild docker containers:
```sh
$ make docker_dev_rebuild
```

run tests in *web* container:
```sh
$ make test
```

Test coverage: 0%

## Links ##

There have not been any links yet.


Generated with [cookiecutter template](https://gl.atomcream.com/omsy/cookiecutter-microservice-template) version {{ cookiecutter.version }}
