-r requirements.txt

factory-boy==3.2.1
ipdb==0.13.9
ipython==8.4.0
coverage==6.4.2
django-extensions==3.2.0
drf-yasg==1.21.3
coreapi==2.3.3
django-silk==5.0.1
django-queryinspect==1.1.0