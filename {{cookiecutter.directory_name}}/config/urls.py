from django.conf.urls import include
from django.urls import path
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
{% if cookiecutter.use_jwt_token -%}
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

{%- endif %}

urlpatterns = [
    path('admin/', admin.site.urls),
{% if cookiecutter.use_drf -%}
    path(r'api_auth/', include('rest_framework.urls', namespace='rest_framework')),
{%- endif %}
{% if cookiecutter.use_jwt_token -%}
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
{%- endif %}
]

if settings.DEBUG:

    from django.urls import re_path
    from rest_framework import permissions
    from drf_yasg.views import get_schema_view
    from drf_yasg import openapi


    urlpatterns += [
        path("admin/silk/", include("silk.urls", namespace="silk_urls")),
    ]
    {% if cookiecutter.use_drf -%}
    schema_view = get_schema_view(
        openapi.Info(
            title="{{cookiecutter.directory_name}} API",
            default_version='v1',
            description="{{cookiecutter.directory_name}} API Documentation",
        ),
        public=True,
        permission_classes=(permissions.AllowAny,),
    )

    urlpatterns += [
        path('api/docs', schema_view.with_ui('swagger', cache_timeout=0),
             name='schema-swagger-ui'),
        re_path(r'api/docs(?P<format>\.json|\.yaml)$',
                schema_view.without_ui(cache_timeout=0),
                name='schema-json'),
        path('api/redoc', schema_view.with_ui('redoc', cache_timeout=0),
             name='schema-redoc'),
        {%- endif %}
    ]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
