import factory  # pylint: disable=import-error

from apps.user.models import User


class UserFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = User

    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    email = factory.Faker('email')
    is_superuser = True
    is_staff = True
    # temp fix for tests, TODO: create custom auth backend
    password = factory.PostGenerationMethodCall('set_password', '1234')

